package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {
    CartiRepoMock cartiRepoMock;
    List<Carte> carti;
    Carte carte;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void cautaCarte_I() {
        //     carti.size = n
        //     autor: ""

        CartiRepoMock cartiRepoMock = new CartiRepoMock();
        Carte carte = new Carte();

        carte.adaugaAutor(" b");
        carte.adaugaCuvantCheie("poezii");
        carte.setAnAparitie("2000");
        carte.setTitlu("Poezii");
        carte.setEditura("Teora");
        cartiRepoMock.adaugaCarte(carte);
        System.out.println("carti gasite :" + cartiRepoMock.getCarti());

        assertEquals(cartiRepoMock.cautaCarte("").isEmpty(),false);
    }

    @Test
    public void cautaCarte_II() {
        //     carti.size = n
        //     autor:cosbuc

        CartiRepoMock cartiRepoMock = new CartiRepoMock();
        Carte carte2 = new Carte();

        carte2.adaugaAutor("cosbuc");
        carte2.adaugaCuvantCheie("poezii");
        carte2.setAnAparitie("2000");
        carte2.setTitlu("Poezii");
        carte2.setEditura("Teora");
        cartiRepoMock.adaugaCarte(carte2);

        System.out.println("carti gasite  :" + cartiRepoMock.getCarti());

        //assertEquals("orice",1,cartiRepoMock.cautaCarte("cosbuc").size());
        assertEquals((cartiRepoMock.cautaCarte("cosbuc").size())!=0,true);
    }


    @Test
    public void cautaCarte_III() {
        //     carti.size = 0
        //     autor:goga


    }

    @After
    public void tearDown() throws Exception {

    }


}