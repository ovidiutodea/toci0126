package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class CartiRepoMockTest1 {

    Carte c1, c2, c3;
    CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void setUpAll() {
        System.out.println("......... before tests............");
    }

    @Before
    public void setUp() throws Exception {
        c1 = new Carte();
        c1.setTitlu("titlu1_1");
        c1.adaugaAutor("Eminescu");
        c1.setAnAparitie("2000");
        c1.setEditura("Teora");
        c1.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poem", "poezie")));

        c2 = new Carte();
        c2.setTitlu("titlu_2");
        c2.adaugaAutor("Goga");
        c2.setAnAparitie("2000");
        c2.setEditura("Teora");
        c2.setCuvinteCheie(new ArrayList<String>(Arrays.asList("poezie", "proza")));

        cartiRepoMock = new CartiRepoMock();
    }

    @After
    public void tearDown() throws Exception {
        c1 = null;
        c2 = null;
        cartiRepoMock = null;

    }



    @Test

    //cautam carte in repo gol

    public void cauta_I() {
        assertEquals("Repository gol confirmat", 0, cartiRepoMock.cautaCarte("Eminescu").size());
        System.out.println("Repository gol confirmat");
    }

    @Test

    //cautam carte dupa un autor existent

    public void cauta_II() {
        cartiRepoMock.adaugaCarte(c1);
        cartiRepoMock.adaugaCarte(c2);
        assertEquals("Autor gasit", 1, cartiRepoMock.cautaCarte("Goga").size());
        System.out.println("Autor gasit");

    }

    @Test

    //cautam carte dupa autor inexistent

    public void cauta_III() {
        cartiRepoMock.adaugaCarte(c1);
        assertEquals("Autorul nu exista", 0, cartiRepoMock.cautaCarte("Noica").size());
        System.out.println("Autorul nu exista");
    }
}