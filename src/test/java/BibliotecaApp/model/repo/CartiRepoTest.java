package BibliotecaApp.model.repo;

import BibliotecaApp.model.Carte;
import org.junit.*;

import static org.junit.Assert.*;


public class CartiRepoTest {
    CartiRepo  cartiRepo;

    Carte carteTest1,carteTest2,carteTest3,carteTest4, carteTest5,
          carteTest6, carteTest7,carteTest8 ;


    @BeforeClass
    public static void setUpBefore(){
        System.out.println("Before class CartiRepo");
    }
    @Before
    public void setUp()  {
        cartiRepo= new CartiRepo();

        // TC 1 == carteTest1
        carteTest1 = new Carte();
        carteTest1.setTitlu("Io");
        carteTest1.adaugaAutor("Marin Sorescu");
        carteTest1.setAnAparitie("1499");
        carteTest1.setEditura("art");
        carteTest1.adaugaCuvantCheie("sorescu");

        // TC 2 == carteTest2
        carteTest2 = new Carte();
        carteTest2.setTitlu("Morometii");
        carteTest2.adaugaAutor("Marin Preda");
        carteTest2.setAnAparitie("1500");
        carteTest2.setEditura("art");
        carteTest2.adaugaCuvantCheie("preda");

        // TC 3 == carteTest3
        carteTest3 = new Carte();
        carteTest3.setTitlu("Morometii");
        carteTest3.adaugaAutor("Marin Preda");
        carteTest3.setAnAparitie("1501");
        carteTest3.setEditura("art");
        carteTest3.adaugaCuvantCheie("preda");

        // TC 4 == carteTest4
        carteTest4 = new Carte();
        carteTest4.setTitlu("Morometii");
        carteTest4.adaugaAutor("Marin Preda");
        carteTest4.setAnAparitie("2017");
        carteTest4.setEditura("art");
        carteTest4.adaugaCuvantCheie("preda");

        // TC 5 == carteTest5
        carteTest5 = new Carte();
        carteTest5.setTitlu("Morometii");
        carteTest5.adaugaAutor("Marin Preda");
        carteTest5.setAnAparitie("2018");
        carteTest5.setEditura("art");
        carteTest5.adaugaCuvantCheie("preda");

        // TC 6 == carteTest6
        carteTest6 = new Carte();
        carteTest6.setTitlu("Morometii");
        carteTest6.adaugaAutor("Marin Preda");
        carteTest6.setAnAparitie("2019");
        carteTest6.setEditura("art");
        carteTest6.adaugaCuvantCheie("preda");

        // TC 7 == carteTest7
        carteTest7 = new Carte();
        carteTest7.setTitlu("M");
        carteTest7.adaugaAutor("Mihail Sadoveanu");
        carteTest7.setAnAparitie("2021");
        carteTest7.setEditura("art");
        carteTest7.adaugaCuvantCheie("sadoveanu");

        // TC 8 == carteTest8
        carteTest8 = new Carte();
        carteTest8.setTitlu("Ion");
        carteTest8.adaugaAutor("Liviu Rebreanu");
        carteTest8.setAnAparitie("2000");
        carteTest8.setEditura("art");
        carteTest8.adaugaCuvantCheie("rebreanu");



    }

    @Test // Expected Exception
    public void adaugaCarte1()  {
       boolean thrown = true;

        try {
            cartiRepo.adaugaCarte(carteTest1);
            System.out.println("The book has been added to the repository");
        } catch (Exception e) {
          thrown = true;
        }

        assertTrue(thrown);
    }

    @Test //Expected NULL
    public void adaugaCarte2() {

        int firstSize= cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(carteTest2);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("Size before to add the book: "+firstSize);
        System.out.println("Size after the book was added :"+lastSize);

        assertNotEquals(firstSize,lastSize);

    }

    @Test //Expected NULL
    public void adaugaCarte3() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(carteTest3);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("Size before to add the book: "+firstSize);
        System.out.println("Size after the book was added :"+lastSize);

        assertNotEquals(firstSize,lastSize);
    }

    @Test //Expected NULL
    public void adaugaCarte4() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(carteTest4);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("Size before to add the book :"+firstSize);
        System.out.println("Size after the book was added :"+lastSize);

        assertNotEquals(firstSize,lastSize);
    }

    @Test //Expected NULL
    public void adaugaCarte5() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(carteTest5);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("Size before to add the book:"+firstSize);
        System.out.println("Size after the book was added :"+lastSize);

        assertNotEquals(firstSize,lastSize);
    }


    @Test // Expected Exception
    public void adaugaCarte6()  {
        boolean thrown = false;

        try {
            cartiRepo.adaugaCarte(carteTest6);
           System.out.println("The book has been added to the repository");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
    }


    @Test // Expected Exception
    public void adaugaCarte7()  {
        boolean thrown = false;

        try {
            cartiRepo.adaugaCarte(carteTest7);
            System.out.println("The book has been added to the repository");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
    }

    @Test //Expected NULL
    public void adaugaCarte8() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(carteTest8);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("Size before to add the book:"+firstSize);
        System.out.println("Size after the book was added :"+lastSize);

        assertNotEquals(firstSize,lastSize);
    }


    @After
    public void tearDown() {
        carteTest1=null;
        carteTest2=null;
        carteTest3=null;
        carteTest4=null;
        carteTest5=null;
        carteTest6=null;
        carteTest7=null;
        carteTest8=null;
    }
    @AfterClass
    public static void teardown(){
        System.out.println("After class CartiRepo");
    }


}